# Matt's Dotfiles (AKA The Linux Cast)

These are my dotfiles. The documentation for them is in progress, I'm trying to do better. Also, my window manager dots change all the time, so if you see a rice you like in a video, but that video is old, the rice is likely to have changed. I do upload everything here, however, so you should be able to view previous commits to get to the rice you want.
